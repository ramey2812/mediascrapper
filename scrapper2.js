const puppeteer = require('puppeteer');
const request = require('request');
const  pageToOpen = process.argv[2];

puppeteer.launch().then(async browser => {
    const page = await browser.newPage();
    page.on('load', async () => {
        const pageRes = await page.content();
        let urls = [];
        urls  = getMediaUrls(pageRes);
        urls.forEach(async url => {
            try {
                const contentType =  await getContentType(url);
                if (contentType != undefined && (contentType.indexOf('audio') >= 0 || contentType.indexOf('video') >= 0)) {
                    console.log(url);
                }
            } catch (err) {
                console.error(err);
            }
        });
        browser.close();
    })
    await page.goto(pageToOpen);
});

function getMediaUrls(html) {
    const urls = []
    let urlStart = html.search('"http');
    let remainingHtml = '';
    let urlEnd;
    let contentType;
    while (urlStart >= 0) {
        remainingHtml = html.substring(urlStart + 1);
        urlEnd = remainingHtml.search('"');
        url = html.substring(urlStart + 1, urlEnd + urlStart + 1);
        urls.push(url)
        html = html.substring(urlEnd + urlStart + 1);
        urlStart = html.search('"http');
    }
    return urls;
}

function getContentType(url) {
    const requestOptions = {
        method: 'HEAD',
        uri: url
    }
    return new Promise((resolve, reject) => {
        request(requestOptions, (err, resp, body) => {
            if (err) {
                return reject(err);
            } else{
                return resolve(resp.headers['content-type'])
            } 
        })
    })
}