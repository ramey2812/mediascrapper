/**
 * @file
 * @arg {string} url where you want to find the media links
 * The script uses Phantomjs to load the webpage. It scrap for all the http/https links in the page content.
 * If the link scrapped ends with any of the popular media formats. It is stored.
 * 
 */


/**
 * Script Dependency
 */
const phantom = require('phantom');

// the array contains some widely used media formatss
let formats = ['.mp3', '.ogg', '.pcm', '.wav', '.aiff', '.aac', '.wma', '.flac', '.alac', '.avi', '.flv', '.mpeg', '.mp4', '.wmv', '.mov', '.flv', '.swf', '.qt']
    , pageToOpen = process.argv[2];

var phInstance = null
    , sitepage = null;

phantom.create()
    .then(instance => {
        phInstance = instance;
        return instance.createPage();
    })
    .then(page => {
        sitepage = page;
        return page.open(pageToOpen)
    })
    .then(status => {
        if (status !== 'success') {
            var err = Error("Failed to get the page");
            return err;
        } else{
            // setting the timeout of 20 seconds to load the page and all the dynamic javascript.
            setTimeout(function() {
                sitepage.evaluate(function(){
                    return document.documentElement.innerHTML
                })
                .then(html => {
                    var urls = getMediaUrls(html);
                    console.log(urls);
                    phInstance.exit();
                    process.exit(0);
                })
                .catch(error => {
                    console.error(error);
                    phInstance.exit();
                });
            }, 20000);
        }
    })
    .catch(error => {
        console.error(error);
        phInstance.exit();
    });

/**
 * @function getMediaUrls takes the html content and finds out media  links.
 * @param {string} html content of the loaded web page.
 * @return {array} of urls that link to a media.
 */
var getMediaUrls = function(html) {
    var urls = []
    var urlStart = html.search('"http');
    var remainingHtml = '';
    var urlEnd;
    while (urlStart >= 0) {
        remainingHtml = html.substring(urlStart + 1);
        urlEnd = remainingHtml.search('"');
        url = html.substring(urlStart + 1, urlEnd + urlStart + 1);
        for (format in formats) {
            if (url.endsWith(formats[format])) {
                urls.push(url);
            }
        }
        html = html.substring(urlEnd + urlStart + 1);
        urlStart = html.search('"http');
    }
    return urls;
}